<?php
echo "array";
$cars = array("Volvo", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
?>

<?php
  echo "indexed array";
$car2 = array("Volvo", "BMW", "Toyota");
echo "I like " . $car2[0] . ", " . $car2[1] . " and " . $car2[2] . ".";
?>


<?php
echo"associative arrays";
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
echo "Peter is " . $age['Peter'] . " years old.";
?>


